Given(/^the project is built$/) do
  `cd ..;mvn clean install package`
end

Given(/^an input string of "([^"]*)"$/) do |search_text|
  @search_text = search_text
end

When("the string is processed") do
  @output = `echo '#{@search_text}' | java -cp ../target/lib/*:../target/* org.allcorn.palindrome.cmdLine`
  puts @output
end

Then("the 3 longest palindromes are extracted") do
  fail = false
  if @output !~ /Text: hijkllkjih, Index: 23, Length: 10/
    fail = true
  end
  if @output !~ /Text: defggfed, Index: 13, Length: 8/
      fail = true
  end
  if @output !~ /Text: abccba, Index: 5, Length: 6/
      fail = true
  end

  if fail
    raise "unexpected output"
  end
end