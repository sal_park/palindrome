Feature: Get palindrome

  Scenario: Extract top 3 palindromes
    Given the project is built
    And an input string of "sqrrqabccbatudefggfedvwhijkllkjihxymnnmzpop"
    When the string is processed
    Then the 3 longest palindromes are extracted