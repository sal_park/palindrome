# Palindrome challenge

## Task
Write an application that finds the 3 longest unique palindromes in a supplied string. For the 3 longest palindromes, report the palindrome, start index and length, in descending order of length.

## How to build

This project uses maven so to build it do something like
```concept
% mvn clean compile
```

And create a package that can be used
```concept
% mvn clean install package
% ls target/*gz
target/palindrome-0.0.1-SNAPSHOT.tar.gz
%
```

## How to run unit tests 

```concept
% mvn clean test
```

## How to run BDD tests

```concept
% cd component-test/
% cucumber features/palindrome.feature
Feature: Get palindrome

Java HotSpot(TM) 64-Bit Server VM warning: ignoring option MaxPermSize=2048m; support was removed in 8.0
  Scenario: Extract top 3 palindromes                                    # features/palindrome.feature:3
    Given the project is built                                           # features/step_definitions/palindromes.rb:1
    And an input string of "sqrrqabccbatudefggfedvwhijkllkjihxymnnmzpop" # features/step_definitions/palindromes.rb:5
    When the string is processed                                         # features/step_definitions/palindromes.rb:9
      Text: hijkllkjih, Index: 23, Length: 10

      Text: defggfed, Index: 13, Length: 8

      Text: abccba, Index: 5, Length: 6
    Then the 3 longest palindromes are extracted                         # features/step_definitions/palindromes.rb:14

1 scenario (1 passed)
4 steps (4 passed)
0m7.411s
```
## How to run

```concept
% which java
/usr/local/jdk1.8.0_162/bin/java
% gzip -d palindrome-0.0.1-SNAPSHOT.tar.gz
% tar -xf palindrome-0.0.1-SNAPSHOT.tar
% cd palindrome-0.0.1-SNAPSHOT/bin/
% echo "sqrrqabccbatudefggfedvwhijkllkjihxymnnmzpop" | ./palindrome
Text: hijkllkjih, Index: 23, Length: 10

Text: defggfed, Index: 13, Length: 8

Text: abccba, Index: 5, Length: 6

%
```
## Comments

In the code I use the following acronyms:

* PD as a shorthand for palindrome (eg abba)

* LHS as a shorthand for the left hand side of a potental PD (eg ab)

* RHS  as a shorthand for the right hand side of a potental PD (eg ba)

