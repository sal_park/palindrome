package org.allcorn.palindrome;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.ListIterator;

public class storePD {

    final static Logger logger = Logger.getLogger(storePD.class);

    //The list of PDs that we have extracted
    ArrayList<palindromeRecord> PDs = new ArrayList<palindromeRecord>();

    //The list of suspect strings that may or may not contain a PD.
    ArrayList<String> possiblePDs = new ArrayList<String>();

    //The list of strings that we have searched but failed to find a PD in.
    ArrayList<String> notPDs = new ArrayList<String>();

    public void addPD(String newPD) {
        palindromeRecord myPD = new palindromeRecord(newPD);
        PDs.add(myPD);
    }

    public void addPossiblePD(String possiblePD) {
        possiblePDs.add(possiblePD);
    }

    public void addNotPD(String notPD) {
        notPDs.add(notPD);
    }

    public ArrayList<palindromeRecord> getPDs() {
        return PDs;
    }

    public ArrayList<String> getPossiblePDs() {
        return possiblePDs;
    }

    public ArrayList<String> getNotPDs() {
        return notPDs;
    }

    //when we are done searching for PDs of a given length we want to make
    //then availabe for the next search at a different PD length
    public void swapNotToPossible() {
        possiblePDs = notPDs;
        notPDs = new ArrayList<String>();
    }

    //record the index for each PD
    public void setIndex(String inputText) {

        ListIterator<palindromeRecord> iter = PDs.listIterator();
        ArrayList<Integer> unique_indexes = new ArrayList<Integer>();

        while(iter.hasNext()) {
            palindromeRecord thisPD = iter.next();
            int index = inputText.indexOf(thisPD.toString());
            if(unique_indexes.indexOf(Integer.valueOf(index)) == -1) {
                //not used this index before
                unique_indexes.add(Integer.valueOf(index)); }
            else {
                //need to look for the next match
                index = inputText.indexOf(thisPD.toString(),index+1); }
            thisPD.setIndex(index);
        }
    }

    //Sort the PDs based on length
    public void sortOnLength() {
        Collections.sort(PDs);
    }

    @Override
    public String toString() {
        return "storePD: " + "PDs=[" + PDs.toString() +
                "] possiblePDs=[" + possiblePDs.toString() +
                "] notPDs=[" + notPDs.toString() + ']';
    }
}
