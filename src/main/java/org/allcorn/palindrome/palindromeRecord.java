package org.allcorn.palindrome;

public class palindromeRecord implements Comparable {

    String PD;
    int index;

    public palindromeRecord(String PD) {
        this.PD = PD;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public int getLength() {
        return PD.length();
    }

    public String prettyPrint() {
        return new String("Text: " + PD + ", Index: " + index + ", Length: " + PD.length());
    }

    @Override
    public String toString() {
        return PD;
    }

    @Override
    public int compareTo(Object o) {
        palindromeRecord otherPD = (palindromeRecord)o;
        return otherPD.getLength() - this.getLength();
    }
}
