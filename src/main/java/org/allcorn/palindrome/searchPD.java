package org.allcorn.palindrome;

import org.apache.log4j.Logger;

import java.util.ListIterator;

public class searchPD {

    final static Logger logger = Logger.getLogger(searchPD.class);
    private storePD myPDS;

    public searchPD(storePD myPDS) {
        this.myPDS = myPDS;
    }

    // @return true if the suspect is a palindromeRecord
    public boolean isPD(String suspect) {

        boolean isEven = false;
        int PDMatchSize;
        int PDLength = suspect.length();

        logger.debug("Suspect PD is [" + suspect + "]");

        if((PDLength % 2) == 0) isEven = true;

        if(isEven) {
            PDMatchSize = PDLength / 2; }
        else {
            PDMatchSize = (PDLength-1) / 2; }

        String LHS_Suspect = suspect.substring(0,PDMatchSize);
        String LHS_SuspectRev = new StringBuilder(LHS_Suspect).reverse().toString();
        String RHS_Suspect = suspect.substring((PDLength-PDMatchSize), PDLength);

        logger.debug(("LHS_Suspect = [" + LHS_Suspect + "] LHS_SuspectRev = [" +
                LHS_SuspectRev + "] RHS_Suspect = [" + RHS_Suspect + "]"));

        return LHS_SuspectRev.equals(RHS_Suspect);
    }

    // Search sourceText for the first palindromeRecord of length PDLength
    // and store results in myPDs
    public void findPD(String sourceText, int PDLength) {

        logger.info("About to check sourceText [" + sourceText + "] for PDs of length [" + PDLength +"]");

        int charPointer = sourceText.length();
        boolean gotPDMatch = false;

        while(charPointer-PDLength >= 0) {
            String suspect = sourceText.substring((charPointer-PDLength),charPointer);
            boolean isPD = isPD(suspect);

            logger.trace("Suspect [" + suspect + "] is PD = [" + isPD + "]");

            if(isPD) {
                gotPDMatch = true;
                String possiblePD = sourceText.substring(0,charPointer-PDLength);
                String notPD = sourceText.substring(charPointer, sourceText.length());

                myPDS.addPD(suspect);

                logger.info("Matched PD of [" + suspect + "]");

                // a PD of this length not found - move to NotPD so we can search it again
                //using different length PDs
                if(notPD.length() > 0) myPDS.addNotPD(notPD);

                // There might be a PD of this length in here - search for it
                if(possiblePD.length() > 0) findPD(possiblePD, PDLength);

                break;
            }

            charPointer--;
        }
        // no match - move sourceText over to NotPD
        if(!gotPDMatch) myPDS.addNotPD(sourceText);
    }

    // Search sourceText for all palindromes of any length
    public void findPD(String sourceText) {

        logger.info("Looking for PDs in string [" + sourceText +"]");

        myPDS.addPossiblePD(sourceText);

        int maxPDlength = sourceText.length();
        for (int PDlength = maxPDlength; PDlength > 1; PDlength--) {

            ListIterator<String> iter = myPDS.getPossiblePDs().listIterator();
            while(iter.hasNext()) {
                findPD(iter.next(), PDlength);
                iter.remove();
            }

            myPDS.swapNotToPossible();
        }

        logger.info("Found the following palindromes: [" + myPDS.getPDs().toString() +"]");
    }
}
