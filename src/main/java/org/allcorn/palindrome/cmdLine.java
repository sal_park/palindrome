package org.allcorn.palindrome;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.ListIterator;

public class cmdLine {

    public static void main(String [ ] args)
    {
        BufferedReader reader = null;
        storePD palindromeStore = new storePD();
        searchPD palindromeSearch = new searchPD(palindromeStore);

        try {
            reader = new BufferedReader(new InputStreamReader(System.in));
            String textToSearch = reader.readLine();
            palindromeSearch.findPD(textToSearch);
            palindromeStore.setIndex(textToSearch);
            palindromeStore.sortOnLength();

            ListIterator<palindromeRecord> iter = palindromeStore.getPDs().listIterator();

            int counter = 0;
            while(iter.hasNext()) {
                palindromeRecord thisPD = iter.next();
                System.out.println(thisPD.prettyPrint() + "\n");
                counter++;
                if(counter>=3) break;
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
