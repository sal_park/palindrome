package org.allcorn.palindrome;

import static org.junit.Assert.*;
import org.junit.Test;
import java.util.ArrayList;
import java.util.Collections;

public class palindromeRecordTest {

    @Test
    public void justPD() {
        palindromeRecord myPD = new palindromeRecord("notlob");
        assertTrue(myPD.toString().equals("notlob"));
    }

    @Test
    public void justIndex() {
        palindromeRecord myPD = new palindromeRecord("notlob");
        myPD.setIndex(3);
        assertTrue(myPD.getIndex() == 3);
    }

    @Test
    public void justLength() {
        palindromeRecord myPD = new palindromeRecord("notlob");
        assertTrue(myPD.getLength() == 6);
    }

    @Test
    public void prettyPrint() {
        palindromeRecord myPD = new palindromeRecord("notlob");
        myPD.setIndex(3);

        assertTrue(myPD.prettyPrint().equals("Text: notlob, Index: 3, Length: 6"));
    }

    @Test
    public void compareOrder() {
        ArrayList<palindromeRecord> PDs = new ArrayList<palindromeRecord>();
        palindromeRecord rec1 = new palindromeRecord("one");
        PDs.add(rec1);
        palindromeRecord rec2 = new palindromeRecord("twoo");
        PDs.add(rec2);
        palindromeRecord rec3 = new palindromeRecord("three");
        PDs.add(rec3);

        assertTrue((PDs.get(0).toString() + PDs.get(1).toString() +
                PDs.get(2).toString()).equals("onetwoothree"));

        Collections.sort(PDs);

        assertTrue((PDs.get(0).toString() + PDs.get(1).toString() +
                PDs.get(2).toString()).equals("threetwooone"));
    }
}
