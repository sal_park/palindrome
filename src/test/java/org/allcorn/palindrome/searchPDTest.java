package org.allcorn.palindrome;

import static org.junit.Assert.*;
import java.util.ArrayList;
import org.junit.Test;

public class searchPDTest {

    @Test
    public void isMatchTestOdd() {
        storePD myStore = new storePD();
        searchPD mySearch = new searchPD(myStore);
        assertTrue(mySearch.isPD("madam"));
    }

    @Test
    public void isMatchTestEven() {
        storePD myStore = new storePD();
        searchPD mySearch = new searchPD(myStore);
        assertTrue(mySearch.isPD("abba"));
    }

    @Test
    public void noMatchTestOdd() {
        storePD myStore = new storePD();
        searchPD mySearch = new searchPD(myStore);
        assertFalse(mySearch.isPD("abcde"));
    }

    @Test
    public void noMatchTestEven() {
        storePD myStore = new storePD();
        searchPD mySearch = new searchPD(myStore);
        assertFalse(mySearch.isPD("abcd"));
    }

    @Test
    public void searchForPDCheckStoreMatchBeginning() {
        storePD myStore = new storePD();
        searchPD mySearch = new searchPD(myStore);
        mySearch.findPD("abbatown",4);

        assertTrue(myStore.getNotPDs().size() == 1);
        assertTrue(myStore.getNotPDs().get(0).equals("town"));

        assertTrue(myStore.getPDs().size() == 1);
        assertTrue(myStore.getPDs().get(0).toString().equals("abba"));
    }

    @Test
    public void searchForPDCheckStoreMatchEnd() {
        storePD myStore = new storePD();
        searchPD mySearch = new searchPD(myStore);
        mySearch.findPD("townabba",4);

        assertTrue(myStore.getNotPDs().size() == 1);
        assertTrue(myStore.getNotPDs().get(0).equals("town"));

        assertTrue(myStore.getPDs().size() == 1);
        assertTrue(myStore.getPDs().get(0).toString().equals("abba"));
    }

    @Test
    public void searchForPDCheckStoreMatchMiddle() {
        storePD myStore = new storePD();
        searchPD mySearch = new searchPD(myStore);
        mySearch.findPD("townabbatoad",4);

        assertTrue(myStore.getNotPDs().size() == 2);
        assertTrue(myStore.getNotPDs().get(0).equals("toad"));
        assertTrue(myStore.getNotPDs().get(1).equals("town"));

        assertTrue(myStore.getPDs().size() == 1);
        assertTrue(myStore.getPDs().get(0).toString().equals("abba"));
    }


    @Test
    public void searchForPDInMiddleOfString() {
        storePD myStore = new storePD();
        searchPD mySearch = new searchPD(myStore);
        mySearch.findPD("abcabbadef",4);

        assertTrue(myStore.getPDs().get(0).toString().equals("abba"));
        assertTrue(myStore.getNotPDs().size() == 2);
        assertTrue(myStore.getNotPDs().get(0).equals("def"));
        assertTrue(myStore.getNotPDs().get(1).equals("abc"));


    }

    @Test
    public void noPDfound() {
        storePD myStore = new storePD();
        searchPD mySearch = new searchPD(myStore);
        mySearch.findPD("abcdef",4);

        assertTrue(myStore.getPDs().size() == 0);

    }

    @Test
    public void PDatend() {
        storePD myStore = new storePD();
        searchPD mySearch = new searchPD(myStore);
        mySearch.findPD("abcdefabba",4);

     //   assertTrue(myStore.getPossiblePDs().get(0).equals("abcdef"));
        assertTrue(myStore.getNotPDs().size() == 1);
        assertTrue(myStore.getNotPDs().get(0).equals("abcdef"));
        assertTrue(myStore.getPDs().get(0).toString().equals("abba"));

    }

    @Test
    public void PDatstart() {
        storePD myStore = new storePD();
        searchPD mySearch = new searchPD(myStore);
        mySearch.findPD("madamabcdef",5);

        assertTrue(myStore.getNotPDs().get(0).equals("abcdef"));
        assertTrue(myStore.getPossiblePDs().size() == 0);
        assertTrue(myStore.getPDs().get(0).toString().equals("madam"));

    }

    @Test
    public void PDgetfirst() {
        storePD myStore = new storePD();
        searchPD mySearch = new searchPD(myStore);
        mySearch.findPD( "mumdad",3);

        assertTrue(myStore.getPDs().size() == 2);
        assertTrue(myStore.getPDs().get(0).toString().equals("dad"));
        assertTrue(myStore.getPDs().get(1).toString().equals("mum"));

    }

    @Test
    public void PDgetMultiplePDs() {
        storePD myStore = new storePD();
        searchPD mySearch = new searchPD(myStore);
        mySearch.findPD("mumdadmadamabba");

        assertTrue(myStore.getPDs().size() == 4);
        assertTrue(myStore.getPDs().get(0).toString().equals("madam"));
        assertTrue(myStore.getPDs().get(1).toString().equals("abba"));
        assertTrue(myStore.getPDs().get(2).toString().equals("dad"));
        assertTrue(myStore.getPDs().get(3).toString().equals("mum"));

    }


    @Test
    public void PDgetMultiplePDsWithCruftEitherEnd() {
        storePD myStore = new storePD();
        searchPD mySearch = new searchPD(myStore);
        mySearch.findPD("xvqmumdadmadamabbafgc");

        assertTrue(myStore.getPDs().size() == 4);
        assertTrue(myStore.getPDs().get(0).toString().equals("madam"));
        assertTrue(myStore.getPDs().get(1).toString().equals("abba"));
        assertTrue(myStore.getPDs().get(2).toString().equals("dad"));
        assertTrue(myStore.getPDs().get(3).toString().equals("mum"));

    }

    @Test
    public void PDgetMultiplePDsWithCruftEitherEndAndMiddle1() {
        storePD myStore = new storePD();
        searchPD mySearch = new searchPD(myStore);
        mySearch.findPD("xvqmumdadmadamoutabbafgc");

        assertTrue(myStore.getPDs().size() == 4);
        assertTrue(myStore.getPDs().get(0).toString().equals("madam"));
        assertTrue(myStore.getPDs().get(1).toString().equals("abba"));
        assertTrue(myStore.getPDs().get(2).toString().equals("dad"));
        assertTrue(myStore.getPDs().get(3).toString().equals("mum"));

    }

    @Test
    public void PDgetMultiplePDsWithCruftEitherEndAndMiddle2() {
        storePD myStore = new storePD();
        searchPD mySearch = new searchPD(myStore);
        mySearch.findPD("xvqmumdadmoutadamabbafgc");

        assertTrue(myStore.getPDs().size() == 3);
        assertTrue(myStore.getPDs().get(0).toString().equals("mdadm"));
        assertTrue(myStore.getPDs().get(1).toString().equals("abba"));
        assertTrue(myStore.getPDs().get(2).toString().equals("ada"));
    }
}
