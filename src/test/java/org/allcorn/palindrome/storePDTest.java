package org.allcorn.palindrome;

import static org.junit.Assert.*;
import org.junit.Test;

public class storePDTest {

    @Test
    public void storePD() {
        storePD myStore = new storePD();

        myStore.addPD("madam");
        myStore.addPD("abba");
        myStore.addPossiblePD("poss");
        myStore.addNotPD("xyz");

        assertTrue(myStore.getPDs().get(0).toString().equals("madam"));
        assertTrue(myStore.getPDs().size() == 2);
        assertTrue(myStore.getPossiblePDs().get(0).toString().equals("poss"));
        assertTrue(myStore.getNotPDs().get(0).toString().equals("xyz"));
    }

    @Test
    public void checkIndex() {

        storePD myStore = new storePD();
        String inputText = "dad123mum123bob";

        myStore.addPD("dad");
        myStore.addPD("mum");
        myStore.addPD("bob");

        myStore.setIndex(inputText);

        System.out.println("index =[" + myStore.getPDs().get(2).getIndex() + "]");
        assertTrue(myStore.getPDs().get(0).getIndex() == 0);
        assertTrue(myStore.getPDs().get(1).getIndex() == 6);
        assertTrue(myStore.getPDs().get(2).getIndex() == 12);
    }

    @Test
    public void get2PDsThatAreTheSameCheckIndex() {
        String testPD = "poptestpop";
        storePD myStore = new storePD();
        searchPD mySearch = new searchPD(myStore);
        mySearch.findPD(testPD);
        myStore.setIndex(testPD);

        assertTrue(myStore.getPDs().get(0).prettyPrint().equals("Text: pop, Index: 0, Length: 3"));
        assertTrue(myStore.getPDs().get(1).prettyPrint().equals("Text: pop, Index: 7, Length: 3"));
    }


    @Test
    public void get2PDsThatAreTheSame2CheckIndex() {
        String testPD = "sqrrqabccbatudefggfedvwhijkllkjihxymnnmzpop";
        storePD myStore = new storePD();
        searchPD mySearch = new searchPD(myStore);
        mySearch.findPD(testPD);
        myStore.setIndex(testPD);

        assertTrue(myStore.getPDs().get(0).prettyPrint().equals("Text: hijkllkjih, Index: 23, Length: 10"));
        assertTrue(myStore.getPDs().get(1).prettyPrint().equals("Text: defggfed, Index: 13, Length: 8"));
        assertTrue(myStore.getPDs().get(2).prettyPrint().equals("Text: abccba, Index: 5, Length: 6"));
    }

}
